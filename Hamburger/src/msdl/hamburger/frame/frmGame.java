package msdl.hamburger.frame;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import javax.management.Query;
import javax.swing.*;

import msdl.hamburger.main;
import msdl.hamburger.panel.*;

public class frmGame extends JFrame implements ActionListener{
	//필요 패널들


	Timer timer;//게임시작과 동시에 작동하는 30초짜리 제한시간 타이머	
	Timer qtimer;//새로운 문제가 생길때마다 작동하는 6초짜리 타이머

	Image imgBackground;

	JPanel pnl;

	frmScore fScore;
	frmTotalScore fTotalScore;

	public static int stage = 1;

	int timersec;
	int qtimersec;

	ArrayList<hamburgerType> aHamburger = new ArrayList<hamburgerType>();//유형이 hamburgerType인 내가 만드는 햄버거 ArrayList 
	ArrayList<hamburgerType> qHamburger = new ArrayList<hamburgerType>();//유형이 hamburgerType인 랜덤으로 돌아가는 문제생성햄버거 ArrayList

	pnlHeart pHeart;
	pnlScore pScore;
	pnlQuestion pQuestion;
	pnlTimer pTimer;
	pnlAnswer pAnswer;
	pnlPad pPad;



	//햄버거 종류 (열거형 1부터시작) 숫자로 쓰지 않기 위해 열거형사용
	enum hamburgerType{LowerBun, Patty, Lettuce, Cheese, UpperBun};

	public frmGame(){

		setImage();

		setSize(600,600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);



		//패널생성
		setPanel();

		//패널위치지정
		pScore.setLocation(502,-2); //점수패널위치지정
		pQuestion.setLocation(0,100);
		pTimer.setLocation(-10,455);
		pAnswer.setLocation(212,330);
		pPad.setLocation(400,400);

		pnl.add(pScore);
		pnl.add(pHeart);
		pnl.add(pQuestion);
		pnl.add(pTimer);
		pnl.add(pAnswer);
		pnl.add(pPad);
		
		add(pnl);

		//pnl.add()

		//패널등록



		//크기조절방지
		setResizable(false);

		addKeyListener(new ListenKey());

		startGame();
	}

	private void setImage() {
		// TODO Auto-generated method stub
		imgBackground = Toolkit.getDefaultToolkit().createImage(main.path + "\\res\\frmgamebackground.png");


	}

	public void startGame(){
		timer = new  Timer(1000, this);//1초지날때마다 --;
		timer.start();
		qtimer = new Timer(1000, new Qtimer());	//제한시간 타이머 생성
		pScore.makeScore();


		if(stage == 1){
			//타이머
			timersec = 30;						
			newQuestion((int)(Math.random()*2+2));
			pHeart.endHeart(5);

		}
		else if(stage == 2){

			timersec = 20;
			newQuestion((int)(Math.random()*2+3));
			pHeart.endHeart(3);		

		}
		else if(stage == 3){

			timersec = 15;			
			newQuestion((int)(Math.random()*2+4));
			pHeart.endHeart(2);

		}		

	}

	public void newQuestion(int count){
		qHamburger.clear();	//기존의 문제 없애 줌(어레이리스트 클리어)
		pQuestion.pAnswer.resetPlate();

		aHamburger.clear();//기존에 내가 쌓은 햄버거 없애줌

		qHamburger.add(hamburgerType.LowerBun); //아래빵고정
		pQuestion.pAnswer.setLowerBun();

		for(int i=0; i<count; i++){ //count는 햄버거 몇단인지
			switch((int)(Math.random()*3+1)){ //재료 세가지 랜덤
			case 1:
				qHamburger.add(hamburgerType.Cheese);
				pQuestion.pAnswer.setCheese();
				break;
			case 2:
				qHamburger.add(hamburgerType.Patty);
				pQuestion.pAnswer.setPatty();
				break;
			case 3:
				qHamburger.add(hamburgerType.Lettuce);
				pQuestion.pAnswer.setLettuce();
				break;
			}
		}
		qHamburger.add(hamburgerType.UpperBun);//위에빵고정
		pQuestion.pAnswer.setUpperBun();
		pQuestion.pAnswer.repaint();
		qtimersec = 0;	//타이머 값 초기화
		qtimer.start();	//타이머 스타트
	}

	private void setPanel() {


		pnl = new JPanel(){
			public void paintComponent(Graphics g){
				super.paintComponent(g);
				g.drawImage(imgBackground, 0, 0,this);
			}
		};
		pnl.setLayout(null);
		pnl.setSize(600,600);
		
		pHeart = new pnlHeart(pnl);
		pScore = new pnlScore(pnl);
		pQuestion = new pnlQuestion(pnl);
		pTimer = new pnlTimer(pnl);
		pAnswer = new pnlAnswer(pnl);
		pPad = new pnlPad(pnl);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {		//제한시간 30초
		// TODO Auto-generated method stub

		fScore = new frmScore();
		fTotalScore = new frmTotalScore();

		timersec--;
		if(timersec==0){//30초가지나면
			JFrame frmScore = new JFrame();
			//			int stage1Good = pScore.CountGood();
			//			int stage1Perfect =pScore.CountPerfect();

			timer.stop();//타이머를 멈추고 ***->Score화면 나오게
			qtimer.stop();

			if(pHeart.heart!=-1){
				if(stage==3){
					fTotalScore.setVisible(true);					

				}
				else{
					fScore.setVisible(true);
					setVisible(false);
				}

			}

		}

		pTimer.setTime(timersec);

	}

	class Qtimer implements ActionListener{//문제가 나올때마다 동작하는 6초짜리 타이머.***문제생성될때마다 반응하게만들기
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub

			qtimersec++;

			if(qtimersec==6){//6초가지나면
				qtimer.stop();//타이머를 멈추고
				pHeart.decHeart();//못맞췄으니까 하트 하나씩 깎아버리기
				pQuestion.pAnswer.resetPlate(); //접시초기화
				newQuestion(2);//새로운문제생성

			}
			pQuestion.setTime(qtimersec);
		}

	}
	class ListenKey implements KeyListener{ //재료를 쌓기위한 키보드리스너.
		@Override
		public void keyPressed(KeyEvent arg0) {
			// TODO Auto-generated method stub
			if(arg0.getKeyCode() == KeyEvent.VK_LEFT)
			{
				pAnswer.setLettuce(); //pAnswer안에 재료넣기.
				aHamburger.add(hamburgerType.Lettuce); //눈에보이게끔 접시위에 올려놓기.

			}
			else if(arg0.getKeyCode() == KeyEvent.VK_RIGHT)
			{
				pAnswer.setCheese();
				aHamburger.add(hamburgerType.Cheese);
			}
			else if(arg0.getKeyCode() == KeyEvent.VK_UP){
				if(aHamburger.contains(hamburgerType.LowerBun)){					
					pAnswer.setUpperBun();
					aHamburger.add(hamburgerType.UpperBun);	
					//aHamburger.clear();
				}

				else{
					pAnswer.setLowerBun();	
					aHamburger.add(hamburgerType.LowerBun);					
				}	

			}		

			else if(arg0.getKeyCode() == KeyEvent.VK_DOWN){	
				pAnswer.setPatty();
				aHamburger.add(hamburgerType.Patty);
			}
			else if(arg0.getKeyCode() == KeyEvent.VK_CONTROL){	//접시초기화
				aHamburger.clear();

				pAnswer.resetPlate();
			}
			else if(arg0.getKeyCode() == KeyEvent.VK_SPACE){//qHamburger와 aHamburger비교
				System.out.println("Compare!");
				compare(qHamburger, aHamburger);
				if(pHeart.heart==-1){
					setVisible(false);
				}
			}

		}


		private void compare(ArrayList<hamburgerType> qHamburger, ArrayList<hamburgerType> ahamburger){//랜덤햄버거와 내가 쌓은 햄버거 비교
			// TODO Auto-generated method stub	

			if(ahamburger.size() == qHamburger.size()){ //사이즈비교
				for(int i=0; i<qHamburger.size(); i++){//사이즈가 같을때
					if(qHamburger.get(i) == ahamburger.get(i)){} //내용물비교

					else {//문제랑 내가 쌓은햄버거가 다르면
						aHamburger.clear();
						newQuestion(2);
						pHeart.decHeart(); //새로운문제생성(접시초기화)
						pAnswer.resetPlate();
						return;
					}
				}
				if(qtimersec < 3){//3초이내 성공시 perfect
					pScore.CountPerfect();
					newQuestion(2);					
				}
				else{
					pScore.CountGood();
					newQuestion(2);					
				}			

			}			
			else {//사이즈조차 다를때 
				aHamburger.clear();
				newQuestion(2);
				pHeart.decHeart();//새로운문제생성(접시초기화)
			}
			pAnswer.resetPlate();			

		}

		@Override
		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void keyTyped(KeyEvent arg0) {
			// TODO Auto-generated method stub
		}

	}
}
