package msdl.hamburger.frame;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import msdl.hamburger.ImageButton;
public class frmStart extends JFrame{
	
	JButton btnStart;
	JButton btnExit;
	JButton btnMethod;
	JPanel pnl;
	frmGame fGame;
	
	Image imgBackground;
	Image imgStart;
	Image imgMethod;
	Image imggameMethod;
	
	BufferedImage image = new BufferedImage(470, 470,
			BufferedImage.TYPE_INT_ARGB);

	public frmStart(){
		
		Toolkit tool = Toolkit.getDefaultToolkit();
		imgBackground = tool.createImage(new File("").getAbsolutePath() + "\\res\\frmstartbackground.png");
		imgStart = tool.createImage(new File("").getAbsolutePath() + "\\res\\btnstart.png");
		imgMethod = tool.createImage(new File("").getAbsolutePath() + "\\res\\btnmethod.png");
		
		setSize(600,500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		setResizable(false);


		//버튼 생성
		setButton();

		//패널 설정
		setPanel(imgBackground);
		
		//버튼 위치 지정
		btnStart.setLocation(120, 300);	
		btnMethod.setLocation(290, 300);
		

		//버튼 화면에 등록
		add(pnl);
		pnl.add(btnStart);
		pnl.add(btnMethod);

		//이미지
		pnl.repaint();
		pnl.validate();
	}

	void setButton(){
		btnStart = new ImageButton(imgStart);
		btnMethod = new ImageButton(imgMethod);
		btnMethod.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				String path = new File("").getAbsolutePath() + "\\res\\gamemethod.png";
				 try {
					image = ImageIO.read( new File(path));
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				// TODO Auto-generated method stub
				
				JFrame gameMethod = new JFrame();
				gameMethod.setResizable(false);
				setLayout(null);
				JPanel pgameMethod;
				
				
				pgameMethod = new JPanel(){
					public void paintComponent(Graphics g){
						super.paintComponent(g);
						g.drawImage(image, 0, 0, null);
					}
				};
				
				pgameMethod.setLayout(null);
				pgameMethod.setSize(480,460);
				//이미지
				pgameMethod.repaint();
				pgameMethod.validate();
				gameMethod.add(pgameMethod);
	
				gameMethod.setSize(480,460);
		
				gameMethod.setVisible(true);
				
	
		
				
			}
			
		});
		btnStart.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0){
				// TODO Auto-generated method stub
				fGame = new frmGame();
				fGame.setVisible(true);
				setVisible(false);
			}
		});
		btnExit = new JButton("그만하기");
	}
	void setPanel(final Image i){
		pnl = new JPanel(){
			public void paintComponent(Graphics g){
				super.paintComponent(g);
				g.drawImage(i, 0, 0, this);
			}
		};
		pnl.setLayout(null);
		pnl.setSize(600,600);
	}
}

