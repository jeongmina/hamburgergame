package msdl.hamburger.frame;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.*;

import msdl.hamburger.main;
import msdl.hamburger.panel.*;

public class frmScore extends JFrame{
	
	pnlEndScore pEndScore;
	pnlScoreButton pScoreButton;
	JPanel pnl;
	JButton btnStop;
	public JButton btnNext;
	
	Image imgBackground;
	Toolkit tool;
	
	public frmScore(){
		tool = Toolkit.getDefaultToolkit();
		setImage();
		
		setSize(600,600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		
		setPanel();
		setButton();
				
		pScoreButton.setLocation(0,500);
		pEndScore.setLocation(0,0);
		
		btnStop.setBounds(50, 500, 200, 60);
		btnNext.setBounds(350, 500, 200, 60);
	
		pnl.add(btnStop);
		pnl.add(btnNext);
		pnl.add(pScoreButton);
		add(pnl);
		
		setResizable(false);		
	}
	private void setImage() {
		// TODO Auto-generated method stub
		imgBackground = tool.createImage(main.path + "\\res\\frmscorebackground.png"); //점수프레임 배경넣기
	}
	private void setButton() { 
		// TODO Auto-generated method stub
		btnNext = new JButton("다음단계로");
		
		btnNext.addActionListener(new ActionListener(){ //다음단계로 버튼을 눌렀을때 반응하는 액션리스너
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				frmGame.stage++; //stage가 ++되면서 +1단계가 됨.
				frmGame fGame = new frmGame(); //새로운게임프레임
				
				fGame.setVisible(true);
			
				setVisible(false);
			}
		});
		
		btnStop = new JButton("그만하기");
		btnStop.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				pnl.invalidate();
				pnl.repaint();
			}
		});
		setVisible(false);		
	}

	private void setPanel() {
		// TODO Auto-generated method stub
		
		pEndScore = new pnlEndScore();
		pScoreButton = new pnlScoreButton();	
		
		pnl = new JPanel(){
			public void paintComponent(Graphics g){
				super.paintComponent(g);
				
				g.drawImage(imgBackground, 0, 0, this); //이미지넣기
			}
		};
		pnl.setLayout(null);
		pnl.setSize(600,600);
	}
}
