package msdl.hamburger.frame;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.*;

import msdl.hamburger.main;
import msdl.hamburger.panel.*;

public class frmTotalScore extends JFrame{
	
	public static int totalGood = 0;
	public static int totalPerfect = 0;
	public static int totalScore = 0;
	
	pnlScore pScore;
	pnlTotalScoreButton pTotalScoreButton;
	pnlTotalScore pTotalScore;
	JButton btnStop;
	JButton btnReplay;
	JPanel pnl;
	

	
	Image imgBackground;
	Toolkit tool;
	public frmTotalScore(){
		tool = Toolkit.getDefaultToolkit();
		setImage();
		setSize(600,600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		
		setPanel();
		setButton();
		
		pTotalScore.setLocation(400,0);
		pTotalScoreButton.setLocation(0,500);		
		pScore.setLocation(0,0);
		pScore.setSize(400,500);
		
		btnStop.setBounds(50, 500, 200, 60);
		btnReplay.setBounds(350, 500, 200, 60);
		

		add(pTotalScoreButton);
		add(pScore);
		pnl.add(btnStop);
		pnl.add(btnReplay);
		pnl.add(pTotalScore);
		
		add(pnl);
		
		
		setResizable(false);		
		
	}	
	
	private void setImage() {
		// TODO Auto-generated method stub
		imgBackground = tool.createImage(main.path + "\\res\\frmTotalscorebackground.png"); //점수프레임 배경넣기
		
	}
	private void setButton() {
		// TODO Auto-generated method stub
		btnReplay = new JButton("다시하기");
		btnReplay.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				frmGame fGame = new frmGame();
				fGame.setVisible(true);
				setVisible(false);				
			}			
		});
		btnStop = new JButton("그만하기");
		setVisible(false);
		
	}
	private void setPanel() {
		// TODO Auto-generated method stub
		
		pTotalScore = new pnlTotalScore();
		pTotalScoreButton = new pnlTotalScoreButton();
		pScore = new pnlScore();
//		얘가 문제야!!
		
		pnl = new JPanel(){
			public void paintComponent(Graphics g){
				super.paintComponent(g);
				g.drawImage(imgBackground, 0, 0, null);
			}
		};
		pnl.setLayout(null);
		pnl.setSize(600,600);
		
		
	}
}
