package msdl.hamburger.panel;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;
import java.util.ArrayList;

import javax.swing.*;

import msdl.hamburger.frame.frmGame;
import msdl.hamburger.frame.frmScore;
import msdl.hamburger.frame.frmTotalScore;

public class pnlHeart extends JPanel{
	JPanel pnl;
	JLabel txtHeart;

	public int heart;

	public pnlHeart(JPanel pnl){
		this.pnl = pnl;
		txtHeart = new JLabel();
		this.add(txtHeart);
		setSize(300,100);
		//this.setBackground(Color.white);
		this.endHeart(5);
	}


	public void  paintComponent(Graphics g){
		pnl.repaint();
		pnl.invalidate();
		for(int i=0; i<heart; i++){
			Image heart1 =new ImageIcon("heart.png").getImage();
			g.drawImage(heart1, 20+(i*35), 20, 30, 30, this);
		}
	}
	public void decHeart(){
		frmScore fScore = new frmScore();
		frmTotalScore fTotalScore = new frmTotalScore();

		if(heart+1> 1){ //하트의 수가 1개보다 많으면
			heart--; //하나씩 깎임

		}
		if(heart+1 == 1){ //하트가 한개남아있을땐
			heart--; //마지막하나를 깎고 

			if(frmGame.stage == 3){//만약에 3단계끝이면 최종score를 불러와야함.
				fTotalScore.setVisible(true);
			}
			//게임오버(frmScore 불러오기),*** 대신 다음단계 버튼못누르게
			else{
				fScore.btnNext.setVisible(false);
				fScore.setVisible(true);
			}

		}
		makeHeart();
	}
	//	public void incHeart(){		
	//		if(heart<=4){
	//			heart++;
	//		}
	//		makeHeart();
	//		
	//	}
	public void endHeart(int heart)/*초기화(하트5개)*/{
		this.heart = heart;
		makeHeart();
	}
	public void makeHeart(){
		//txtHeart.setText(heart+"");
	}
	//	public void paintComponent(Graphics g){
	//		Image heart1 =new ImageIcon("heart.png").getImage();
	//		g.drawImage(heart1, 10, 10, 20, 20, this);
	//		
	//		Image heart2 =new ImageIcon("heart.png").getImage();
	//		g.drawImage(heart2, 10, 10, 20, 20, null);
	//		
	//		Image heart3 =new ImageIcon("heart.png").getImage();
	//		g.drawImage(heart3, 10, 10, 20, 20, null);
	//		
	//		Image heart4 =new ImageIcon("heart.png").getImage();
	//		g.drawImage(heart4, 10, 10, 20, 20, null);
	//	
	//		Image heart5 =new ImageIcon("heart.png").getImage();
	//		g.drawImage(heart5, 10, 10, 20, 20, null);
	//		
	//	}

}
