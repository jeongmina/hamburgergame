package msdl.hamburger.panel;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import msdl.hamburger.main;

public class pnlAnswer extends JPanel{
	JPanel pnl;	

	Image imgLettuce;
	Image imgLowerBun;
	Image imgUpperBun;
	Image imgCheese;
	Image imgPatty;

	BufferedImage image = new BufferedImage(200, 30,
			BufferedImage.TYPE_INT_ARGB);
	String path;

	ArrayList<hamburgerType> aryHamburgerType = new ArrayList<hamburgerType>();
	enum hamburgerType {UpperBun, LowerBun, Cheese, Patty, Lettuce};

	public void paintComponent(Graphics g){
		if(pnl!=null){
			pnl.invalidate();
			pnl.repaint();
		}
		int GAP = 0;
		for(int i=0; i<aryHamburgerType.size(); i++){
			
			final int defX = 5;
			final int defY = 150;
			switch(aryHamburgerType.get(i)){
			case Lettuce:
				g.drawImage(imgLettuce, defX-5, defY-GAP, null);
				GAP+=30;
				break;
			case Patty:
				g.drawImage(imgPatty, defX, defY-GAP, null);
				GAP+=20;
				break;
			case Cheese:
				g.drawImage(imgCheese, defX-5, defY-GAP+15, null);
				GAP+=10;
				break;
			case LowerBun:
				g.drawImage(imgLowerBun, defX, defY-GAP, null);
				GAP+=20;
				break;
			case UpperBun:
				g.drawImage(imgUpperBun, defX, defY-GAP-10, null);
				GAP+=20;
				break;
			}
		}
	}
	public pnlAnswer(JPanel pnl){
		setImage();
		this.pnl = pnl;
		setSize(200,200);
		this.setBackground(Color.YELLOW);
	}
	void setImage(){
		imgUpperBun = Toolkit.getDefaultToolkit().createImage(main.path + "\\res\\upperbun.png");
		imgLowerBun = Toolkit.getDefaultToolkit().createImage(main.path + "\\res\\lowerbun.png");
		imgCheese = Toolkit.getDefaultToolkit().createImage(main.path + "\\res\\cheese.png");
		imgPatty = Toolkit.getDefaultToolkit().createImage(main.path + "\\res\\patty.png");
		imgLettuce = Toolkit.getDefaultToolkit().createImage(main.path + "\\res\\lettuce.png");
	}
	public void setLowerBun(){
		aryHamburgerType.add(hamburgerType.LowerBun);
		reDraw();
	}
	public void setUpperBun(){
		aryHamburgerType.add(hamburgerType.UpperBun);
		reDraw();
	}
	public void setCheese(){
		aryHamburgerType.add(hamburgerType.Cheese);
		reDraw();
	}
	public void setPatty(){
		aryHamburgerType.add(hamburgerType.Patty);
		reDraw();
	}
	public void setLettuce(){
		aryHamburgerType.add(hamburgerType.Lettuce);
		reDraw();
	}
	public void endHeart(){

	}
	public void resetPlate(){
		//���� �״� �ܹ��� ����
		aryHamburgerType.clear();
		reDraw();
	}
	void reDraw(){
		invalidate();
		repaint();
	}
}


