package msdl.hamburger.panel;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.*;

public class pnlPad extends JPanel{
	JPanel pnl;
	public pnlPad(JPanel pnl){
		this.pnl = pnl;
		setSize(200,200);
		this.setBackground(Color.PINK);
	}
	public void paintComponent(Graphics g){
		pnl.invalidate();
		pnl.repaint();
	}

}
