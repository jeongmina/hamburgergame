package msdl.hamburger.panel;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.*;

import msdl.hamburger.frame.frmGame;
import msdl.hamburger.frame.frmTotalScore;

public class pnlScore extends JPanel{
	JPanel pnl;
	int total=0; //원래점수
	JLabel TextScore;
	int Good, Perfect;

	public pnlScore(JPanel pnl){
		this.pnl = pnl;
		
		setSize(300,100);
		TextScore = new JLabel();
		TextScore.setFont(new Font("휴먼매직", Font.BOLD, 40));
		this.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		this.add(TextScore);
		makeScore();
	}	
	public pnlScore() {
		// TODO Auto-generated constructor stub
	}
	public void CountGood(){
		Good++;//Good의 갯수를 ++해준뒤

		frmTotalScore.totalGood++;

		if(frmGame.stage == 1){
			plusScore(3);//Good에 해당하는 점수를 넣어서 setScore()메소드 호출.
		}
		else if(frmGame.stage == 2){
			plusScore(5);
		}
		else{
			plusScore(7);
		}
	}
	public void CountPerfect(){
		Perfect++;//Perfect의 갯수를 ++해준뒤

		frmTotalScore.totalPerfect++;

		if(frmGame.stage == 1){
			plusScore(5);//Perfect에 해당하는 점수를 넣어서 setScore()메소드 호출.
		}
		else if(frmGame.stage == 2){
			plusScore(7);
		}
		else{
			plusScore(10);
		}
	}
	public int getGood() {
		return Good;
	}
	public void setGood(int good) {
		Good = good;
	}
	public int getPerfect() {
		return Perfect;
	}
	public void setPerfect(int perfect) {
		Perfect = perfect;
	}
	public void plusScore(int Score){	//원래있던 점수:total, Score는 문제맞출때마다 올라가는 점수
		total = Score + total;

		frmTotalScore.totalScore += Score;

		makeScore();			
	}
	public void makeScore(){
		TextScore.setText(total + "");

	}
	
	public void paintComponent(Graphics g){
		//pnl.invalidate();
		//pnl.repaint();
	}
	

}
