package msdl.hamburger.panel;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.*;

public class pnlQuestion extends JPanel{
	JPanel pnl;
	public pnlAnswer pAnswer;
	JLabel txtQuestionTime;
	
	public pnlQuestion(JPanel pnl){
		this.pnl = pnl;
		setSize(600,300);
		this.setBackground(Color.GREEN);
		setLayout(null);
		
		txtQuestionTime = new JLabel();
//		txtQuestionTime.setBackground(Color.black);
		txtQuestionTime.setSize(10, 20);
		txtQuestionTime.setLocation(50, 20);
		pAnswer = new pnlAnswer(this);
		pAnswer.setLocation(50,50);
		
		add(txtQuestionTime);
		add(pAnswer);
	}
	public void setTime(int time){
		//txtQuestionTime.setText(time+"");
	}
	public void paintComponent(Graphics g){
		pnl.repaint();
		pnl.invalidate();
	}

}
