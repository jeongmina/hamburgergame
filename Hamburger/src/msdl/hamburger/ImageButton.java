package msdl.hamburger;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class ImageButton extends JButton{
	public ImageButton(Image img){
		setIcon(new ImageIcon(img));
		setSize(img.getWidth(null), img.getHeight(null));
		setBorderPainted(false);	
		setContentAreaFilled(false);
	}

}
